sap.ui.define([
	'sap/m/MessageToast',
	"sap/ui/core/mvc/Controller"
], function (MessageToast, Controller) {
	"use strict";

	return Controller.extend("Bachelorproef_2018-2019.Bachelorproef_2018-2019.controller.App", {
		onInit: function() {
			
		},
		onItemSelected: function(oEvent) {
			var oSelectedItem = oEvent.getParameter("listItem");
			
			var msg = oSelectedItem.getTitle() + " is " + this.calculateYearFromNow(oSelectedItem.getIntro()) + " jaar oud!";
			MessageToast.show(msg);
		},
		
		calculateYearFromNow: function(jaar) {
			return (new Date().getFullYear() - jaar);
		}
	});
});