/* global QUnit */
QUnit.config.autostart = false;

sap.ui.getCore().attachInit(function () {
	"use strict";

	sap.ui.require([
		"Bachelorproef_2018-2019/Bachelorproef_2018-2019/test/unit/AllTests"
	], function () {
		QUnit.start();
	});
});