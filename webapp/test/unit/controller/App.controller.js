/*global QUnit*/

sap.ui.define([
	"Bachelorproef_2018-2019/Bachelorproef_2018-2019/controller/App.controller"
], function (Controller) {
	"use strict";

	QUnit.module("App Controller");

	QUnit.test("I should test the App controller", function (assert) {
		var oAppController = new Controller();
		oAppController.onInit();
		assert.ok(oAppController);
	});
	
	QUnit.test("Test of calculate the year method", function(assert) {
		// arrangement
		var oController = new Controller();

		// action

		// assertions
		assert.equal(oController.calculateYearFromNow(2017), 2);
	});

});