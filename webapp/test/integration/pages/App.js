sap.ui.define([
	"sap/ui/test/Opa5",
	"sap/ui/test/matchers/AggregationFilled"
], function (Opa5, AggregationFilled) {
	"use strict";
	
	var sViewName = "App",
		sListId = "artiestenList";

	Opa5.createPageObjects({
		onTheAppPage: {

			actions: {},

			assertions: {

				iShouldSeeTheApp: function () {
					return this.waitFor({
						id: "app",
						viewName: sViewName,
						success: function () {
							Opa5.assert.ok(true, "The App view is displayed");
						},
						errorMessage: "Did not find the App view"
					});
				},
				
				iWaitUntilTheTableIsLoaded : function () {
					return this.waitFor({
						id : sListId,
						viewName : sViewName,
						matchers : [ new AggregationFilled({name : "items"}) ],
						errorMessage : "The List has not been loaded"
					});
				},

				iWaitUntilTheListIsNotVisible : function () {
					return this.waitFor({
						id : sListId,
						viewName : sViewName,
						visible: false,
						matchers : function (oList) {
							// visible false also returns visible controls so we need an extra check here
							return !oList.$().is(":visible");
						},
						errorMessage : "The List is still visible"
					});
				}
			}
		}
	});

});